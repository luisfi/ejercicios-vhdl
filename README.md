# Prácticas VHDL

Prácticas en VHDL para la asignatura de Arquitectura de Computadoras de la
Facultad de Ingeniería de la UNAM.

## Contenido

* [Contador de 4 bits](practica1/)

  Se desarrolla un contador de 4 bits.

* [Implementación de una Carta ASM con VHDL](practica2/)

  A partir de una carta ASM proporcionada, realizar su implementación en
  el lenguaje VHDL.

* [Direccionamiento](practica3/)

  Se implementan dos modos de direccionamiento:

  * Entrada-Estado
  * Implícito

* [Secuenciador](practica4/)

  Se implementa un secuenciador básico.

* [Micro68HC11](practica5/)

  Implementación del micro 68HC11 de motorola.

* [Direccionamiento implícito](practica6/)

  Implementación del direccionamiento implícito.

* [Pipeline](pipeline/)
  
  Implementación del micro en modo pipeline.


## Licencia

  [GPLv3](COPYING)
